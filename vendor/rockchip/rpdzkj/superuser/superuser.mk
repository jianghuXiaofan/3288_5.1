CUR_PATH := vendor/rockchip/rpdzkj/superuser


PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/superuser/superuser.sh:system/xbin/superuser.sh
PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/superuser/nosuperuser.sh:system/xbin/nosuperuser.sh
PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/superuser/root_enable/install-recovery.sh:system/bin/install-recovery.sh
modeswitch_files := $(shell ls $(CUR_PATH)/root_enable)
PRODUCT_COPY_FILES += \
    $(foreach file, $(modeswitch_files), \
    $(CUR_PATH)/root_enable/$(file):system/usr/superuser/root_enable/$(file))
modeswitch_files := $(shell ls $(CUR_PATH)/root_disable)
PRODUCT_COPY_FILES += \
    $(foreach file, $(modeswitch_files), \
    $(CUR_PATH)/root_disable/$(file):system/usr/superuser/root_disable/$(file))