#!/system/bin/sh
if [ ! -f "/system/lib/libdummy.so" ]; then
    cp /system/usr/superuser/root_enable/libdummy.so /system/lib/libdummy.so
fi
if [ ! -f "/system/xbin/daemonsu" ]; then
    cp /system/usr/superuser/root_enable/daemonsu /system/xbin/daemonsu
fi
if [ ! -f "/system/bin/install-recovery.sh" ]; then
    cp /system/usr/superuser/root_enable/install-recovery.sh /system/bin/install-recovery.sh
fi
    cp /system/usr/superuser/root_enable/su /system/xbin/su
    
# If you're implementing this in a custom kernel/firmware,
# I suggest you use a different script name, and add a service
# to launch it from init.rc

# Launches SuperSU in daemon mode only on Android 4.3+.
# Nothing will happen on 4.2.x or older, unless SELinux+Enforcing.
# If you want to force loading the daemon, use "--daemon" instead

/system/xbin/daemonsu --auto-daemon &

# Some apps like to run stuff from this script as well, that will 
# obviously break root - in your code, just search this file
# for "install-recovery-2.sh", and if present, write there instead.

/system/etc/install-recovery-2.sh