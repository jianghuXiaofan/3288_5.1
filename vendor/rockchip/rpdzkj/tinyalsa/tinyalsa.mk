CUR_PATH := vendor/rockchip/rpdzkj/tinyalsa


PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/tinyalsa/tinycap:system/bin/tinycap
PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/tinyalsa/tinymix:system/bin/tinymix
PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/tinyalsa/tinypcminfo:system/bin/tinypcminfo
PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/tinyalsa/tinyplay:system/bin/tinyplay
PRODUCT_COPY_FILES += vendor/rockchip/rpdzkj/tinyalsa/bootsound.sh:system/bin/bootsound.sh
