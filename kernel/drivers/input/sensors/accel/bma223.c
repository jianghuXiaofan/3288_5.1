/* drivers/input/sensors/access/bma223.c
 *
 * Copyright (C) 2012-2015 ROCKCHIP.
 * Author: luowei <lw@rock-chips.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/miscdevice.h>
#include <linux/gpio.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/freezer.h>
#include <linux/of_gpio.h>
#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif
#include <linux/sensor-dev.h>

/* Default register settings */
//#define BMA223_REG_INTSRC	    		0xC //RO

#define BMA2X2_RANGE_SET                3  /* +/- 2G */
#define BMA2X2_RANGE_2G                 3
#define BMA2X2_RANGE_4G                 5
#define BMA2X2_RANGE_8G                 8
#define BMA2X2_RANGE_16G                12

#define BMA2X2_RANGE_SEL__POS             0
#define BMA2X2_RANGE_SEL__LEN             4
#define BMA2X2_RANGE_SEL__MSK             0x0F
#define BMA2X2_RANGE_SEL__REG             BMA2X2_RANGE_SEL_REG
#define BMA2X2_RANGE_SEL_REG                    0x0F

//#define BMA223_RATE_SHIFT		  3
#define BMA223_PRECISION       10
#define BMA223_BOUNDARY        (0x1 << (BMA223_PRECISION -1))
#define BMA223_GRAVITY_STEP    BMA_RANGE / BMA223_BOUNDARY

#define BMA223_REG_X_OUT_LSB       	0x2 //RO
#define BMA223_REG_X_OUT_MSB       	0x3 //RO
#define BMA223_REG_Y_OUT_LSB       	0x4 //RO
#define BMA223_REG_Y_OUT_MSB       	0x5 //RO
#define BMA223_REG_Z_OUT_LSB       	0x6 //RO
#define BMA223_REG_Z_OUT_MSB       	0x7 //RO
#define BMA223_REG_WHO_AM_I      		0x0 //RO

#define BMA2X2_BW_SET                   12 /* 125HZ  */
#define BMA2X2_BW_7_81HZ        0x08
#define BMA2X2_BW_15_63HZ       0x09
#define BMA2X2_BW_31_25HZ       0x0A
#define BMA2X2_BW_62_50HZ       0x0B
#define BMA2X2_BW_125HZ         0x0C
#define BMA2X2_BW_250HZ         0x0D
#define BMA2X2_BW_500HZ         0x0E
#define BMA2X2_BW_1000HZ        0x0F
#define BMA2X2_BW_SEL_REG                       0x10
#define BMA2X2_BANDWIDTH__POS             0
#define BMA2X2_BANDWIDTH__LEN             5
#define BMA2X2_BANDWIDTH__MSK             0x1F
#define BMA2X2_BANDWIDTH__REG             BMA2X2_BW_SEL_REG

#define FREAD_MASK				0 /* enabled(1<<1) only if reading MSB 8bits*/
#define BMA_RANGE			2000000
#define BMA223_DEVID      		0x223 //myself define
//#define BMA223_ENABLE		1


#define BMA2X2_SET_BITSLICE(regvar, bitname, val)\
         ((regvar & ~bitname##__MSK) | ((val<<bitname##__POS)&bitname##__MSK))

static int bma2x2_smbus_read_byte(struct i2c_client *client,
                unsigned char reg_addr, unsigned char *data)
{
        s32 dummy;
        dummy = sensor_read_reg(client, reg_addr);
        if (dummy < 0)
                return -1;
        *data = dummy & 0x000000ff;

        return 0;
}

static int bma2x2_smbus_write_byte(struct i2c_client *client,
                unsigned char reg_addr, unsigned char *data)
{
        s32 dummy;

        dummy = sensor_write_reg(client, reg_addr, *data);
        if (dummy < 0)
                return -1;
        return 0;
}
/****************operate according to sensor chip:start************/

static int sensor_active(struct i2c_client *client, int enable, int rate)
{
	return 0;
//	struct sensor_private_data *sensor =
//	    (struct sensor_private_data *) i2c_get_clientdata(client);	
//	int result = 0;
//	int status = 0;
//
//	sensor->ops->ctrl_data = sensor_read_reg(client, sensor->ops->ctrl_reg);
//	
//	//register setting according to chip datasheet		
//	if(enable)
//	{	
//		status = BMA223_ENABLE;	//bma223
//		sensor->ops->ctrl_data |= status;	
//	}
//	else
//	{
//		status = ~BMA223_ENABLE;	//bma223
//		sensor->ops->ctrl_data &= status;
//	}
//
//	DBG("%s:reg=0x%x,reg_ctrl=0x%x,enable=%d\n",__func__,sensor->ops->ctrl_reg, sensor->ops->ctrl_data, enable);
//	result = sensor_write_reg(client, sensor->ops->ctrl_reg, sensor->ops->ctrl_data);
//	if(result)
//		printk("%s:fail to active sensor\n",__func__);
//
//	
//	return result;

}

int bma2x2_set_bandwidth(struct i2c_client *client, unsigned char BW)
{
        int comres = 0;
        unsigned char data = 0;
        int Bandwidth = 0;

        if (BW > 7 && BW < 16) {
                switch (BW) {
                case BMA2X2_BW_7_81HZ:
                        Bandwidth = BMA2X2_BW_7_81HZ;

                        /*  7.81 Hz      64000 uS   */
                        break;
                case BMA2X2_BW_15_63HZ:
                        Bandwidth = BMA2X2_BW_15_63HZ;

                        /*  15.63 Hz     32000 uS   */
                        break;
                case BMA2X2_BW_31_25HZ:
                        Bandwidth = BMA2X2_BW_31_25HZ;

                        /*  31.25 Hz     16000 uS   */
                        break;
                case BMA2X2_BW_62_50HZ:
                        Bandwidth = BMA2X2_BW_62_50HZ;

                        /*  62.50 Hz     8000 uS   */
                        break;
                case BMA2X2_BW_125HZ:
                        Bandwidth = BMA2X2_BW_125HZ;

                        /*  125 Hz       4000 uS   */
                        break;
                case BMA2X2_BW_250HZ:
                        Bandwidth = BMA2X2_BW_250HZ;

                        /*  250 Hz       2000 uS   */
                        break;
                case BMA2X2_BW_500HZ:
                        Bandwidth = BMA2X2_BW_500HZ;

                        /*  500 Hz       1000 uS   */
                        break;
                case BMA2X2_BW_1000HZ:
                        Bandwidth = BMA2X2_BW_1000HZ;

                        /*  1000 Hz      500 uS   */
                        break;
                default:
                        break;
                }
                comres = bma2x2_smbus_read_byte(client, BMA2X2_BANDWIDTH__REG,
                                &data);
                data = BMA2X2_SET_BITSLICE(data, BMA2X2_BANDWIDTH, Bandwidth);
                comres += bma2x2_smbus_write_byte(client, BMA2X2_BANDWIDTH__REG,
                                &data);
        } else {
                comres = -1 ;
        }

        return comres;
}

static int bma2x2_set_range(struct i2c_client *client, unsigned char Range)
{
        int comres = 0 ;
        unsigned char data1 = 0;

        if ((Range == 3) || (Range == 5) || (Range == 8) || (Range == 12)) {
                comres = bma2x2_smbus_read_byte(client, BMA2X2_RANGE_SEL_REG,
                                &data1);
                switch (Range) {
                case BMA2X2_RANGE_2G:
                        data1  = BMA2X2_SET_BITSLICE(data1,
                                        BMA2X2_RANGE_SEL, 3);
                        break;
                case BMA2X2_RANGE_4G:
                        data1  = BMA2X2_SET_BITSLICE(data1,
                                        BMA2X2_RANGE_SEL, 5);
                        break;
                case BMA2X2_RANGE_8G:
                        data1  = BMA2X2_SET_BITSLICE(data1,
                                        BMA2X2_RANGE_SEL, 8);
                        break;
                case BMA2X2_RANGE_16G:
                        data1  = BMA2X2_SET_BITSLICE(data1,
                                        BMA2X2_RANGE_SEL, 12);
                        break;
                default:
                        break;
                }
                comres += bma2x2_smbus_write_byte(client, BMA2X2_RANGE_SEL_REG,
                                &data1);
        } else {
                comres = -1 ;
        }

        return comres;
}


static int sensor_init(struct i2c_client *client)
{
	int ret = 0; 
	int i = 0;      
	struct sensor_private_data *sensor =
	    (struct sensor_private_data *) i2c_get_clientdata(client);

	
	ret = sensor->ops->active(client,0,0);
	if(ret)
	{
		printk("%s:line=%d,error\n",__func__,__LINE__);
		return ret;
	}
	
	sensor->status_cur = SENSOR_OFF;

	bma2x2_set_bandwidth(client, BMA2X2_BW_SET);
	bma2x2_set_range(client, BMA2X2_RANGE_SET);

	sensor->devid = BMA223_DEVID;

	return ret;
}

static int sensor_convert_data(struct i2c_client *client, char high_byte, char low_byte)
{
    s64 result;
	struct sensor_private_data *sensor =
	    (struct sensor_private_data *) i2c_get_clientdata(client);	

	switch (sensor->devid) {
		case BMA223_DEVID:			
			swap(high_byte,low_byte);
			result = ((int)high_byte << (BMA223_PRECISION-8)) 
					| ((int)low_byte >> (16-BMA223_PRECISION));
			if (result < BMA223_BOUNDARY)
			      result = result* BMA223_GRAVITY_STEP;
			else{
				result = ~( ((~result & (0x7fff>>(16-BMA223_PRECISION)) ) + 1) 
						* BMA223_GRAVITY_STEP) + 1;
			}
			break;
		default:
			printk(KERN_ERR "%s: devid wasn't set correctly    %d\n",__func__,sensor->devid);
			return -EFAULT;

			
    }

    return (int)result;
}

static int gsensor_report_value(struct i2c_client *client, struct sensor_axis *axis)
{
	struct sensor_private_data *sensor =
	    (struct sensor_private_data *) i2c_get_clientdata(client);	

	/* Report acceleration sensor information */
	input_report_abs(sensor->input_dev, ABS_X, axis->x);
	input_report_abs(sensor->input_dev, ABS_Y, axis->y);
	input_report_abs(sensor->input_dev, ABS_Z, axis->z);
	input_sync(sensor->input_dev);
	DBG("Gsensor x==%d  y==%d z==%d\n",axis->x,axis->y,axis->z);

	return 0;
}

#define GSENSOR_MIN  		10
static int sensor_report_value(struct i2c_client *client)
{
	struct sensor_private_data *sensor =
		(struct sensor_private_data *) i2c_get_clientdata(client);	
	struct sensor_platform_data *pdata = sensor->pdata;
	int ret = 0;
	int x,y,z;
	struct sensor_axis axis;
	char buffer[6] = {0};	
	char value = 0;
	int bma_sensor_orientation[3] = {0};

	if(sensor->ops->read_len < 6)	//sensor->ops->read_len = 6
	{
		printk("%s:lenth is error,len=%d\n",__func__,sensor->ops->read_len);
		return -1;
	}
	
	memset(buffer, 0, 6);
	
	/* Data bytes from hardware xL, xH, yL, yH, zL, zH */	
	do {
		*buffer = sensor->ops->read_reg;
		ret = sensor_rx_data(client, buffer, sensor->ops->read_len);
		if (ret < 0)
		return ret;
	} while (0);


	//this gsensor need 6 bytes buffer
	x = sensor_convert_data(sensor->client, buffer[0], buffer[1]);	//buffer[0]:high bit 
	y = sensor_convert_data(sensor->client, buffer[2], buffer[3]);
	z = sensor_convert_data(sensor->client, buffer[4], buffer[5]);		

	axis.x = (pdata->orientation[0])*x + (pdata->orientation[1])*y + (pdata->orientation[2])*z;
	axis.y = (pdata->orientation[3])*x + (pdata->orientation[4])*y + (pdata->orientation[5])*z;
	axis.z = (pdata->orientation[6])*x + (pdata->orientation[7])*y + (pdata->orientation[8])*z;

	DBG( "%s: axis = %d  %d  %d \n", __func__, axis.x, axis.y, axis.z);

	//Report event only while value is changed to save some power
	if((abs(sensor->axis.x - axis.x) > GSENSOR_MIN) || (abs(sensor->axis.y - axis.y) > GSENSOR_MIN) || (abs(sensor->axis.z - axis.z) > GSENSOR_MIN))
	{
		gsensor_report_value(client, &axis);

		/* ����ػ�������. */
		mutex_lock(&(sensor->data_mutex) );
		sensor->axis = axis;
		mutex_unlock(&(sensor->data_mutex) );
	}

//	if((sensor->pdata->irq_enable)&& (sensor->ops->int_status_reg >= 0))	//read sensor intterupt status register
//	{
//		
//		value = sensor_read_reg(client, sensor->ops->int_status_reg);
//		DBG("%s:sensor int status :0x%x\n",__func__,value);
//	}
	
	return ret;
}


struct sensor_operate gsensor_bma223_ops = {
	.name				= "bma223",
	.type				= SENSOR_TYPE_ACCEL,			//sensor type and it should be correct
	.id_i2c				= ACCEL_ID_BMA223,				//i2c id number
	.read_reg			= BMA223_REG_X_OUT_LSB,		//read data
	.read_len			= 6,							//data length
	.id_reg				= SENSOR_UNKNOW_DATA,			//read device id from this register
	.id_data 			= BMA223_DEVID,//SENSOR_UNKNOW_DATA,			//device id
	.precision			= BMA223_PRECISION,			//10 bit
//	.int_status_reg 		= BMA223_REG_INTSRC,			//intterupt status register
	.range				= {-BMA_RANGE,BMA_RANGE},	//range
	.active				= sensor_active,	
	.init				= sensor_init,
	.report 			= sensor_report_value,
};

/****************operate according to sensor chip:end************/

//function name should not be changed
static struct sensor_operate *gsensor_get_ops(void)
{
	return &gsensor_bma223_ops;
}


static int __init gsensor_bma223_init(void)
{
	struct sensor_operate *ops = gsensor_get_ops();
	int result = 0;
	int type = ops->type;
	result = sensor_register_slave(type, NULL, NULL, gsensor_get_ops);	
	return result;
}

static void __exit gsensor_bma223_exit(void)
{
	struct sensor_operate *ops = gsensor_get_ops();
	int type = ops->type;
	sensor_unregister_slave(type, NULL, NULL, gsensor_get_ops);
}


module_init(gsensor_bma223_init);
module_exit(gsensor_bma223_exit);



