/*
 * (C) Copyright 2010
 * rpdzkj
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/gpio.h>
#include <linux/gpio.h>
#include <linux/proc_fs.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <asm/uaccess.h>


struct gpio_config {
	struct gpio *gpios;	//用于保存nr_gpios个GPIO
	int nr_gpios;	//GPIO个数
};

/* 表示一个GPIO */
struct gpio_label{
	char *label;	//label,用于从设备树找到GPIO
	int io;	//IO序号，与开发板丝印保持一致
};

static struct gpio_config *pgpio_config;
static unsigned int read_a_gpio;

#define IO1 0	//GPIO5_C0_U	io1
#define IO2 1	//GPIO8_A3_U	io2
#define IO3 2	//GPIO7_A4_U	io3
#define IO4 3	//GPIO5_C1_U	io4
#define IO5 4	//GPIO5_B3_U	io5
#define IO6 5	//GPIO5_B2_U	io6
#define IO7 6	//GPIO0_B3_D	io7
#define IO8 7	//GPIO5_B4_U	io8
#define IO9 8	//GPIO7_C5_D	io9
#define IOa 9	//GPIO6_B3_d	ioa
#define IO319 319//GPIO15_d7	io319

#define IO_MIN 0
#define IO_MAX 319	//rk3288最大GPIO为319

static struct gpio_label gpio_array[] = {
	{.label = "gpio5-c0", .io = IO1},
	{.label = "gpio8-a3", .io = IO2},
	{.label = "gpio7-a4", .io = IO3},
	{.label = "gpio5-c1", .io = IO4},
	{.label = "gpio5-b3", .io = IO5},
	{.label = "gpio5-b2", .io = IO6},
	{.label = "gpio0-b3", .io = IO7},
	{.label = "gpio5-b4", .io = IO8},
	{.label = "gpio7-c5", .io = IO9},
	{.label = "gpio6-b3", .io = IOa},
	//{.label = "gpio15-d7", .io = IO319},
};

#define BUF_MAX sizeof(gpio_array)/sizeof(gpio_array[0])

static int gpio_setup(struct gpio_config *gpio_conf)
{
	int num, i;	
	struct gpio *temp_gpios;
	
	if(NULL == gpio_conf){
		printk("can not get gpio config\n");
		return -ENODATA;
	}
	num = gpio_conf->nr_gpios;
	
	temp_gpios = gpio_conf->gpios;		
	if(NULL == temp_gpios){
		printk("gpio can not find\n");
		return -ENODEV;
	}

	for(i = 0; i < num; i++){
		if(temp_gpios[i].gpio < IO_MIN || temp_gpios[i].gpio > IO_MAX){
			printk("continue temp_gpios[%d].gpio\n", i, temp_gpios[i].gpio);
			continue;			
		}
		gpio_request(temp_gpios[i].gpio, "rpdzkj-gpio");
	}

	return 0;
}
	
static ssize_t proc_write	(struct file *file, const char __user *buffer,
				 size_t count, loff_t *ppos)

{
	int value, i, num, is_high;
	unsigned int gpio_nr;
	char *buf;
	struct gpio *temp_gpios;

	num = pgpio_config->nr_gpios;	
	temp_gpios = pgpio_config->gpios;

	buf = (char *)kzalloc(count + 1, GFP_KERNEL);
	if(NULL == buf){
		printk("can not kzalloc mem to buf\n");
		return -ENOMEM;
	}

	if (copy_from_user((void *)buf, buffer, count)){
		printk("failed to copy data from user space\n");
		return -EFAULT;
	}

	sscanf(buf, "%d", &value);
	
	//printk("==================copy from user value = %d ======================\n", value);	
	//sscanf(buffer, "%d", &value); //can not use sscanf to get data from user space, using copy_from_user instead.
	
	gpio_nr = value / 2;
	is_high = value % 2;
	
	//printk("value = %d gpio_nr = %d is_high = %d\n", value, gpio_nr, is_high);
	
	if(gpio_nr > num){
		printk("wrong gpio to set\n");
		return -ENODEV;
	}

	if(NULL == temp_gpios){
		printk("gpio can not find\n");
		return -ENODEV;
	}
	
	if(temp_gpios[gpio_nr].gpio >= IO_MIN && temp_gpios[gpio_nr].gpio <= IO_MAX){
		gpio_direction_output(temp_gpios[gpio_nr].gpio, is_high);
		read_a_gpio = gpio_nr;
	}
	else
		printk("can not find gpio : %d\n", temp_gpios[gpio_nr].gpio);

	//return 0;
	kfree(buf);
	
	return count;

}

static ssize_t proc_read(struct file *file, char __user *user_buf, 
				size_t count, loff_t *ppos)

{
	int i, num, value = 0, len = 0;	
	char buf[BUF_MAX] = {2};
	struct gpio *temp_gpios;

	//buf[BUF_MAX - 1] = '\n';
	num = pgpio_config->nr_gpios;	
	temp_gpios = pgpio_config->gpios;

	/*buf = (char *)kmalloc(num * 2, GFP_KERNEL);
	if(NULL == buf){
		printk("failed to kmalloc mem space to buf\n");
		return -ENOMEM;
	}*/
	//memset(buf, 2, sizeof(buf));	
	//printk("gpio = %d value = %d buf = %s\n", read_a_gpio, value, buf);	

	if(read_a_gpio > IO_MAX || read_a_gpio < IO_MIN){
		for(i = 0; i < num; i++){
			if(temp_gpios[i].gpio < IO_MIN || temp_gpios[i].gpio > IO_MAX){
				printk("%d do not exit in device tree\n", temp_gpios[i].gpio);
				//continue;
				value = 2;
			} 
			else{
				value = gpio_get_value(temp_gpios[i].gpio);
			}
			
			if(i < num - 1)
				len += sprintf(buf+len, "%d,", value);
			else
				len += sprintf(buf+len, "%d\n", value);	
		}
	}
	else{
		value = gpio_get_value(temp_gpios[read_a_gpio].gpio);
		len  += sprintf(buf+len, "%d", value);
		read_a_gpio = IO_MAX + 1;
	}

	if(copy_to_user(user_buf, (void *)buf, len)){
		printk("failed to copy data to user space\n");
	    return -EFAULT;
	}

	return len;
}

static int proc_open(struct inode *inode, struct file *file)
{
	return 0;
}

static const struct file_operations rp_gpio_ctrl_operations = {
	.owner		= THIS_MODULE,
	.open		= proc_open,
	.read		= proc_read,
	.write		= proc_write,

};

static int rpdzkj_gpio_ctrl_probe(struct platform_device *pdev)
{
	struct device_node *np = NULL;
	struct gpio *pgpio;
	unsigned int temp_gpio;
	int i, gpio_array_num = sizeof(gpio_array)/sizeof(gpio_array[0]);
	enum of_gpio_flags flags;
	struct proc_dir_entry *proc_rpdzkj, *proc_rp_gpio_ctrl;

	printk("%s start\n", __func__);

	if (!pdev->dev.of_node) {
		printk("device tree node not found\n");
		return -ENODEV;
	}
	np = pdev->dev.of_node;

	pgpio_config = (struct gpio_config *)kmalloc(sizeof(struct gpio_config), GFP_KERNEL);
	if(NULL == pgpio_config){
		printk("failed to kmalloc mem space to pgpio_config\n");
		return -ENOMEM;
	}
	np->data = pgpio_config;
	
	pgpio = (struct gpio *)kzalloc(sizeof(struct gpio) * gpio_array_num, GFP_KERNEL);
	if(NULL == pgpio){
		printk("failed to kmalloc mem space to pgpio\n");
		return -ENOMEM;
	}
	
	i = 0;
	while(i++ < gpio_array_num)
		pgpio[i].gpio = IO_MAX + 1;
		
	pgpio_config->gpios = pgpio;
	pgpio_config->nr_gpios = 0;
	
	for(i = 0; i < gpio_array_num; i++){
		temp_gpio= of_get_named_gpio_flags(np, gpio_array[i].label, 0, &flags);
		if(temp_gpio < IO_MIN || temp_gpio > IO_MAX){
			printk("%s do not exit in device tree\n",gpio_array[i].label);
			continue;
		}
		pgpio[gpio_array[i].io].gpio = temp_gpio;
		pgpio[gpio_array[i].io].flags = flags;
		pgpio[gpio_array[i].io].label = gpio_array[i].label;
		pgpio_config->nr_gpios++;
		//printk("IO = %d, %s : %d\n",gpio_array[i].io, gpio_array[i].label, pgpio[gpio_array[i].io].gpio );
	}

	proc_rpdzkj = proc_mkdir("rpdzkj", NULL);
	if (!proc_rpdzkj)
		goto err1;

	proc_rp_gpio_ctrl = proc_create("rpdzkj/rp_gpio_ctrl", 0666, NULL, &rp_gpio_ctrl_operations);
	if (!proc_rp_gpio_ctrl)
		goto err2;

	if(gpio_setup(pgpio_config) < 0)
		printk("gpio_setup error\n");

	
	printk("%s end\n", __func__);
	return 0;

err2:
	remove_proc_entry("rpdzkj", NULL);
err1:
	return -ENOMEM;

}



static const struct of_device_id rpdzkj_gpio_ctrl_match[] = {
	{.compatible = "rpdzkj,gpio-ctrl",},
	{},
};

MODULE_DEVICE_TABLE(of, rpdzkj_gpio_ctrl_match);

static struct platform_driver rpdzkj_gpio_ctrl_driver = {
	.probe		= rpdzkj_gpio_ctrl_probe,
	.driver = {
		.name	= "rpdzkj-gpio-ctrl",
		.owner	= THIS_MODULE,
		.of_match_table = rpdzkj_gpio_ctrl_match,
	},
};

static int __init rp_gpio_init(void)
{
	return platform_driver_register(&rpdzkj_gpio_ctrl_driver);
}

static void __exit rp_gpio_exit(void)
{	
	if(NULL != pgpio_config){
		if(NULL != pgpio_config->gpios)
			kfree(pgpio_config->gpios);
		kfree(pgpio_config);
	}

	remove_proc_entry("rpdzkj/rp_gpio_ctrl", NULL);
		
	platform_driver_unregister(&rpdzkj_gpio_ctrl_driver);
}



module_init(rp_gpio_init);
module_exit(rp_gpio_exit);

MODULE_AUTHOR("rpdzkj");
MODULE_DESCRIPTION("gpio driver for rpdzkj");
MODULE_LICENSE("GPL");
