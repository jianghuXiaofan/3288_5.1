
struct rp_gpio{
	int gpio;
	int enable;
};
struct en_data{
	struct rp_gpio sd_enable_io;
	struct rp_gpio spk_enable_io;
};
struct led_data{
	struct rp_gpio led_gpio;
};
