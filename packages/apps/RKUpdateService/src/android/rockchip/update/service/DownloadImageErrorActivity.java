/*  --------------------------------------------------------------------------------------------------------
 *  Copyright(C), 2010-2011, Fuzhou Rockchip Co. ,Ltd.  All Rights Reserved.
 *  $_FOR_ROCKCHIP_RBOX_$ : add by xubilv
 *  File:   DownloadImageErrorActivity.java
 *
 *  Desc:   
 *          -----------------------------------------------------------------------------------
 *
 *          -----------------------------------------------------------------------------------
 *
 *          -----------------------------------------------------------------------------------
 *  Usage:		
 *
 *  Note:
 *
 *  Author: xubilv
 *
 *  --------------------------------------------------------------------------------------------------------
 *  Version:
 *          v1.0
 *  --------------------------------------------------------------------------------------------------------
 *  Log:
	----Apr 13  2012            v1.0
 *
 *  --------------------------------------------------------------------------------------------------------
 */

package android.rockchip.update.service;

import android.content.IntentFilter;
import android.content.Context;
import android.os.Bundle;
import java.util.Formatter;
import java.util.Locale;

import java.lang.StringBuilder;
import android.os.HandlerThread;
import android.os.Handler;
import android.view.WindowManager;
import android.content.DialogInterface;
import android.content.BroadcastReceiver;
import android.os.Message;
import android.util.Log;
import android.content.Intent;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.ServiceManager;
import android.os.Looper;
import android.app.Dialog;
import java.io.File;
import android.os.Environment;
import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;


public class DownloadImageErrorActivity extends AlertActivity implements DialogInterface.OnClickListener {

    static final String TAG = "DownloadImageErrorActivity";

    private static final boolean DEBUG = true;
    // private static final boolean DEBUG = false;

    private  String mErrorMsg;
    private static void LOG(String msg) {
        if ( DEBUG ) {
            Log.d(TAG, msg);
        }
    }
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        LOG("--------DownloadImageErrorActivity : onCreate() : Entered.");
        
        Bundle extr = getIntent().getExtras();
        mErrorMsg = extr.getString(RKUpdateService.EXTRA_ERR_MESSAGE);

        /* <set up the "dialog".> */
        final AlertController.AlertParams p = mAlertParams;
        p.mIconId = R.drawable.ic_dialog_alert;
        p.mTitle = getString(R.string.CIEA_title);
        p.mMessage = mErrorMsg;

        p.mPositiveButtonText = getString(R.string.NIA_btn_ok);
        p.mPositiveButtonListener = this;
        p.mNegativeButtonText = null;//getString(R.string.IFIA_btn_no);
        p.mNegativeButtonListener = null;//this;

        setupAlert();
    }


    public void onClick(DialogInterface dialog, int which) {
        // No matter what, finish the activity
        finish();
    }
}
